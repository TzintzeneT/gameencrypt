# GameEncrypt

E2EE algorithm for binary data implemented in ruby, created by a person who does not know anything about encryption (me) and was just having fun. Probbably not a good idea to implement it in something serius that acctually requare any tipe of security, but you can do it anyways, because its free & open source (GPLv3).

# Y tho?

I had an idea for how to create an encryption algorithm while working on [AtroChess-moves](https://gitlab.com/TzintzeneT/atrochess-moves) with a game-like position represented by the `@seed` & `@positive` values & its own rules and moves (`action0` & `action1`).
<br/>
Only the "players" (clients) know the position so only they know what would be the meaning of that move, while the position keep changing every move. Because it is possible to get to the same position with a different sequence of moves, knowing what the current position is will not allow you to go backwords.
<br/>

I thught it was cool so I implemented it _FIRST_ in Go (witch was annoying) then in Ruby because I wanted to try it out.
it was fun & a lot faster to write in while having 0 expiriance working in Ruby before.
<br/>

Enjoy looking at this mess.
<br/>

# FOSS

because FOSS is awesome