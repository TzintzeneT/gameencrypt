require './binary'
require './Game'

bData = string_to_binary('TzintzeneT')
eData = []

cGame = Game.new(2212, true, 0)


for i in 0..bData.length - 1

    eWord = []
    
    for j in 0..bData[i].length - 1

        # eData[i][j] = 
        eWord[j] = cGame.apply_value(bData[i][j])
    end

    eData[i] = eWord

end

puts "\nOriginal data:\n#{bData}"
puts "\nEncrypted data:\n#{eData}"