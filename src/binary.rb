
# get a letter an return the binary data of it as an array based on the ascii table
def letter_to_binary(letter)

    letter_number = letter.ord

    size = 1
    index = 0

    binary_data = []

    while size < letter_number

        size *= 2
        index += 1
        
    end

    # puts index

    while size > 0

        if letter_number >= size
            binary_data[index] = 1
            letter_number -= size
        else
            binary_data[index] = 0
        end

        size /= 2
        index -= 1
    end

    while binary_data.length % 4 != 0
        binary_data[binary_data.length] = 0
    end

    binary_data
end


def string_to_binary(str)
    # puts "waaa '#{str}'"
    # puts str[0]

    binary_string = []

    for i in 0..str.length - 1
        
        binary_string[i] = letter_to_binary(str[i])
    end

    binary_string
end

# puts "#{letter_to_binary('=')}"
# puts "#{string_to_binary('Hello, world!')}"