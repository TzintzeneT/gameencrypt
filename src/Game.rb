
class Game

    DOWNGRADE_BAR = 20000

    def initialize(s, pos, ci)

        @seed = s
        @positive = pos
        @counter_intuitive = ci

        @wtf = 0
    end


    # evaluate the current position
    def eval

        # count = 0

        # if seed % 2 == 1
        #     count += 1
        # end

        # if positive
        #     count += 1
        # end

        # if counter_intuitive > 0
        #     count += 1
        # end

        # # puts "count: #{count}"

        # if count > 1
        #     return 1
        # end

        # return 0

        if positive

            # puts "#{seed} + #{counter_intuitive} = #{(seed + counter_intuitive) % 2}"
            return (seed) % 2
        end

        # puts "#{seed} + #{counter_intuitive} + 1 = #{(seed + counter_intuitive + 1) % 2}"
        return (seed + 1) % 2

    end


    # apply a move
    def apply_move(move)

        need_downgrade = seed * 2 >= DOWNGRADE_BAR

        if move == 0
            action0
            # downgrade
        elsif move == 1
            action1
            # downgrade
        end

        if need_downgrade
            downgrade
        end

        eval
    end

    # apply a value
    def apply_value(value)

        temp_game = Game.new(seed, positive, counter_intuitive)
        temp_game.apply_move(0)
        temp_game.debug

        if temp_game.eval == value
            apply_move(0)
            return 0
        end

        temp_game = Game.new(seed, positive, counter_intuitive)
        temp_game.apply_move(1)
        temp_game.debug

        if temp_game.eval == value
            apply_move(1)
            return 1
        end

        puts "wtf"
        debug
        return 2

    end

    def wtf
        @wtf
    end

    # puts a debug string for the giga chads developers
    def debug
        # puts "s: #{seed}, p: #{positive}, ci: #{counter_intuitive}, eval: #{eval}"
    end

    private

    # do move type 0
    def action0

        if (counter_intuitive).abs > 3
            # @counter_intuitive = seed % 2

            # if !positive
            #     # @counter_intuitive *= -1
            # end

            @counter_intuitive -= counter_intuitive + counter_intuitive/(counter_intuitive).abs

            action1
            return
        end

        # if counter_intuitive

        @seed *= 2

        @counter_intuitive -= 1

        # if counter_intuitive > 0
        #     @positive = !positive
        # end
    end

    # do move type 1
    def action1


        if (counter_intuitive).abs > 3
            # @counter_intuitive = seed % 2

            # if positive
            #     # @counter_intuitive *= -1
            # end

            # @counter_intuitive -= counter_intuitive + ((counter_intuitive%2) * - 1)
            @counter_intuitive -= counter_intuitive + counter_intuitive/(counter_intuitive).abs

            action0
            return
        end

        @seed *= 3


        # oVal = eval
        

        @seed -= seed % 10

        # if oVal != eval
        @positive = !positive
        # end

        @counter_intuitive += 1
    end

    def downgrade

        if seed < DOWNGRADE_BAR
            return
        end

        @wtf += 1
        
        targetValue = eval

        if seed % 7 == 0
            @seed /= 9
            
        elsif seed % 6 == 1
            @seed /= 4
            
        elsif seed % 5 == 2
            @seed /= 7
            
        elsif seed % 4 == 3
            @seed /= 8
        
        else 
            @seed /= 5
        end


        # if counter_intuitive >

        # @counter_intuitive = counter_intuitive % 3

        # @seed /= 10

        if targetValue == eval
            # @seed += 1
            @positive = !positive
        end
    end

    def seed
        @seed
    end

    def positive
        @positive
    end

    def counter_intuitive
        @counter_intuitive
    end
end


# player1 = Game.new(719, true, 0)
# player2 = Game.new(719, true, 0)

# data = [1, 0, 0, 0, 0, 1, 1, 0]#[0, 0, 0, 0, 0, 0, 1]#[0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0]#[0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0]#[0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1]#[1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0]#[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]#[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
# moves = []
# uData = []

# similarety = 0

# for index in 0..data.length - 1

#     # player1.debug

#     moves[index] = player1.apply_value(data[index])

#     uData[index] = player2.apply_move(moves[index])

#     if moves[index] == uData[index]
#         similarety += 1
#     end

# end

# puts "#{data}"

# puts "#{moves}"

# puts "#{uData}"

# puts "#{player1.wtf}/#{moves.length}"

# puts "#{similarety}/#{moves.length}"
# player1.debug

# player2.apply_move(player1.apply_value(0))

# player1.debug
# player2.debug